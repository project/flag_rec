; $Id$

Flag Recommender uses the Recommender API module to calculate recommendations based on user flagging activity.

For example, using the default "Bookmarks" flag it can calculate two types of recommendations:

 1. Recommended content based on a user's bookmarks.
 2. Users who bookmarked this node also bookmarked.

Configuration
-------------
The configuration page is at Administer > Site Configuration > Flag recommender.

Display
-------
Use the Views integration patch to Recommender API (issue http://drupal.org/node/673786) to display flag based recommendations.

Note: When creating a view you should add the filter "Recommender: Application ID" to limit the view to a single recommendation result set. The Application ID is listed on the Flag Recommender configuration page.


