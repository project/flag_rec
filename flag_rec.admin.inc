<?php
// $Id$

/**
 * Main flag recommender page with table of configurations.
 */
function flag_rec_admin_page() {
  $configurations = flag_rec_get_configurations();
  return theme('flag_rec_admin_page', $configurations);
}

/**
 * Flag recommender form for adding/editing single configuration.
 */
function flag_rec_settings_form(&$form_state, $id = NULL) {
  $form = array();

  $settings = flag_rec_get_configuration($id);

  $form['#configuration_id'] = $id;

  $form['id'] = array(
    '#title' => t('Identifier'),
    '#type' => 'textfield',
    '#description' => t('Enter a unique identifier for this flag recommender configuration.'),
    '#default_value' => $settings['id'],
    '#required' => TRUE,
  );

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#description' => t('Enter a descriptive label.'),
    '#default_value' => $settings['label'],
    '#required' => TRUE,
  );

  $flags = flag_get_flags();
  $flag_options = array();
  foreach($flags as $fid => $flag) {
    $flag_options[$fid] = $flag->name;
  }

  $form['flag'] = array(
    '#title' => t('Flag'),
    '#type' => 'select',
    '#description' => t('Select the flag from which to calculate recommendations.'),
    '#default_value' => $settings['flag'],
    '#required' => TRUE,
    '#options' => $flag_options,
  );

  $algorithms = flag_rec_get_algorithms();
  $titles = array();
  $descriptions = array();

  foreach($algorithms as $key => $algorithm) {
    $titles[$key] = $algorithm['title'];
    $descriptions[$key] = $algorithm['description'];
  }

  $form['algorithm'] = array(
    '#title' => t('Algorithm'),
    '#type' => 'radios',
    '#options' => $titles,
    '#option_descriptions' => $descriptions,
    '#default_value' => $settings['algorithm'],
    '#required' => TRUE,
    '#description' => t('Select which algorithm to use.'),
  );

  $form['boost_recency'] = array(
    '#title' => t('Boost more recent flags'),
    '#type' => 'checkbox',
    '#default_value' => $settings['boost_recency'],
    '#required' => FALSE,
    '#description' => t('Check to boost more recent flags.'),
  );

  $form['days'] = array(
    '#title' => t('Maximum flag age (days)'),
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => $settings['days'],
    '#required' => FALSE,
    '#description' => t('If set only flags set less than the given number of days ago will be used to calculate recommendations. Leave blank or set to zero for unlimited flag age.'),
  );

  $form['usertype'] = array(
    '#title' => t('User type'),
    '#type' => 'radios',
    '#options' => array('authenticated' => t('Authenticated'), 'anonymous' => t('Anonymous')),
    '#default_value' => $settings['usertype'],
    '#required' => FALSE,
    '#description' => t('Flag recommendations can be calculated for either anonymous or authenticated users.'),
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Validation handler for flag_rec_settings_form.
 */
function flag_rec_settings_form_validate($form, &$form_state) {
  if (!preg_match('/^[a-z_][a-z0-9_]*$/', $form_state['values']['id'])) {
    form_set_error('id', t('The identifier may only contain lowercase letters, underscores, and numbers.'));
  }

  if (empty($form['#configuration_id'])) {
    // new configuration - check that hasn't been used before
    $existing_configuration = flag_rec_get_configuration($form_state['values']['id']);
    if (!empty($existing_configuration)) {
      form_set_error('id', t('The identifier must be unique.'));
    }
  }

  if (!empty($form_state['values']['days'])) {
    $val = $form_state['values']['days'];
    if (!(($val !== true) && ((string)(int) $val) === ((string) $val)) || 0 > (int) $val) {
      form_set_error('days', t('The maximum flag age must be an positive integer.'));
    }
  }
}

/**
 * Submit handler for flag_rec_settings_form.
 */
function flag_rec_settings_form_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  unset($form_state['values']['submit'], $form_state['values']['reset'], $form_state['values']['form_id'], $form_state['values']['op'], $form_state['values']['form_token'], $form_state['values']['form_build_id']);

  flag_rec_set_configuration($form_state['values']['id'], $form_state['values']);

  // this ensures that an entry is added to the recommender app map table
  $recommender_app_id = recommender_get_app_id($form_state['values']['id']);

  $form_state['redirect'] = 'admin/settings/flag-rec';
}

/**
 * Confirmation form for delete configuration operation.
 */
function flag_rec_delete_form(&$form_state, $id) {
  $settings = flag_rec_get_configuration($id);
  $form = array('#frid' => $id);
  $question = t('Delete flag recommender configuration "@name"?', array('@name' => $settings['label']));
  $path = 'admin/settings/flag-rec';

  return confirm_form($form, $question, $path);
}

/**
 * Submit handler for delete configuration confirmation form.
 */
function flag_rec_delete_form_submit($form, &$form_state) {
  $settings = flag_rec_get_configuration($form['#frid']);
  if ($form_state['values']['confirm']) {
    flag_rec_set_configuration($form['#frid']);
  }
  drupal_set_message(t('Flag recommender configuration @name has been deleted.', array('@name' => $settings['label'])));
  $form_state['redirect'] = 'admin/settings/flag-rec';
}

/**
 * Theme function for main flag recommender admin page.
 */
function theme_flag_rec_admin_page($configurations = NULL) {
  $output = '';

  if(empty($configurations)) {
    $output .= t('Not yet configured. <a href="@path">Set up a flag recommender configuration</a>.', array('@path' => url('admin/settings/flag-rec/add')));
  } else {
    foreach($configurations as $configuration) {
      $ops = array(
        'flags_rec_edit' =>  array('title' => t('edit'), 'href' => 'admin/settings/flag-rec/edit/'.$configuration['id'], 'query' => drupal_get_destination()),
        'flags_rec_delete' =>  array('title' => t('delete'), 'href' => 'admin/settings/flag-rec/delete/'.$configuration['id']),
      );

      $flag = flag_get_flag($configuration['flag']);
      $algorithm = flag_rec_get_algorithms($configuration['algorithm']);

      $app_id = Recommender::convertAppId($configuration['id']);

      $rows[] = array(
        $configuration['label'],
        $app_id,
        $flag ? $flag->title : t('Flag "@flag" not found.', array('@flag' => $configuration['flag'])),
        $algorithm['title'],
        theme('links', $ops),
      );
    }

    $header = array(t('Label'), t('Application ID'), t('Flag'), t('Algorithm'), t('Ops'));

    $output .= theme('table', $header, $rows);

    $output .= '<p>'.t('To run the recommender go to the <a href="@path">Recommender API configuration page</a>.', array('@path' => url('admin/settings/recommender'))).'</p>';
    $output .= '<p>'.t('When creating views with the patch at <a href="@path">add views support [to Recommender API]</a> you will need the Application ID listed above to filter the view to a single configuration.', array('@path' => 'http://drupal.org/node/673786')).'</p>';
  }

  return $output;
}
